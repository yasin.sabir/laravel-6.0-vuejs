<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    //

    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            $token = Str::random(80);
            Auth::user()->api_token = $token;
            Auth::user()->save();
            return response()->json(['token' => $token], 200);
        }
        return response()->json(['status' => 'User Email & Password is wrong'], 403);

        //        $email = $request->email;
        //        $pass  = bcrypt($request->password);
        //        $user = User::where(['email'=>$email])->where(['password' => $pass])->first();
        //        if($user){
        //            $token = Hash::make($request->password);
        //            $user->api_token = $token;
        //            $user->save();
        //            return response()->json(['token'=>$token],200);
        //        }
        //        return response()->json(['status'=>'User Email & Password is wrong'],403);
    }

}
